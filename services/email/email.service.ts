import { ConfigurableApp } from "../../../_core/config/ConfigurableApp.interface";
import { ActionResult } from "../../../_core/utils/ActionResult";
import { Email } from "./entities/email";
import nodemailer from "nodemailer";
import { Service, ConfigService } from "../../../_core/decorator";

@Service()
export class EmailService
{
    @ConfigService()
    private configService:ConfigurableApp;
    
    send(email:Email):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>
        {
            let sender=nodemailer.createTransport({
                service:this.configService.getValueOf('mail').service,
                auth:this.configService.getValueOf('mail').auth
            });
            sender.sendMail(email.toString(),(error,infos)=>{
                let result:ActionResult=new ActionResult();
                if(error) {
                    result.resultCode=ActionResult.UNKNOW_ERROR;
                    result.result=error;
                    reject(result);
                }
                else resolve(result);
            })
        });
    }
}