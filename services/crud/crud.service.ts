import { Service, DBPersistence } from "../../../_core/decorator";
import { PersistentEntity } from "../../../_core/persistence/PersistentEntity";
import { PersistenceManager } from "../../../_core/persistence/PersistenceManager.interface";
import { ActionResult } from "../../../_core/utils/ActionResult";

@Service()
export class CrudService
{
    @DBPersistence()
    protected db:PersistenceManager;

    add(entity:PersistentEntity):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>
        {
            this.db.getQueryBuilder(entity).create(entity);
        })
    }
    delete(entity:PersistentEntity):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>
        {
            
        })
    }
    update(entity:PersistentEntity):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>
        {
            
        })
    }
    get(entity:PersistentEntity,params:any):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>
        {
            
        })
    }
}