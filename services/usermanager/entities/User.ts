/**
@author: Cedric nguendap
@description: Cette classe permet represente un utilisateur du systéme
@created 09/10/2020
*/

import { Entity } from "../../../core/Entity";
import { CPersistentEntity } from "../../../core/persistence/CPersistentEntity";
import { EntityID } from "../../../core/utils/EntityID";

export class User extends CPersistentEntity
{
    /**
     * @description nom de l'utilisateur
     * @type String
     */
    public firstname:String="";

    /**
     * @description prenom de l'utilisateur
     * @type String
     */
    public lastname:String="";

    /**
     * @description mot de passe de l'utilisateur
     * @type String
     */
    public password:String="";



    public username:String="";

    public appToken


    constructor(_id:EntityID=new EntityID(),fname:String="",lname:String="",username:String="",pwd:String="")
    {
        super(_id);
        this.firstname=fname;
        this.lastname=lname; 
        this.password=pwd;
        this.username=username;
    } 
}