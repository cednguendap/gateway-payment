import { ApplicationModuleRouting } from "./bussiness/application/application.module";
import { UsersModuleRouting } from "./bussiness/users/users.module";


export const ModulesRouting=[
    ApplicationModuleRouting,
    UsersModuleRouting
]