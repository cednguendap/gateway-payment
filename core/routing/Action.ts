/**
@author Cedric nguendap
@description Cette classe présente une action fait par l'utilisateur a partir
    d'une url
@created 18/10/2020
*/

import { ApplicationEntity } from "../ApplicationEntity";
import { Entity } from "../Entity";


export class Action extends ApplicationEntity
{
    public method:String="";

    public params:any={};

    public action:String="";

    public secure:Boolean=true;

    constructor(
        method:String="",
        action:String="",
        params:any={},
        secure:Boolean=true)
        {
            super();
            this.action=action;
            this.method=method;
            this.params=params;
            this.secure=secure;
        }
    
    isSecure():Boolean
    {
        return this.secure;
    }
    toString():Record<string | number,any> {
        throw new Error("Method not implemented.");
    }
    hydrate(entity: Entity): void {
        throw new Error("Method not implemented.");
    }

}