import { ConfigurationServiceFactory } from "../config/ConfigurationServiceFactory";
import { InjectorContainer } from "../lifecycle/injector_container";

export function ConfigService()
{
    return (target: any, property: string) => {
        target[property] = InjectorContainer.getInstance()
        .getInstanceOf<ConfigurationServiceFactory>(ConfigurationServiceFactory)
        .getInstance();;
    };
}