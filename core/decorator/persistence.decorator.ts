import { InjectorContainer } from "../lifecycle/injector_container";
import { CPersistenceManagerFactory } from "../persistence/CPersistenceManagerFactory";

export function DBPersistence() {
    return (target: any, property: string) => {
        target[property] = InjectorContainer.getInstance()
        .getInstanceOf<CPersistenceManagerFactory>(CPersistenceManagerFactory)
        .getInstance();;
    };
}
