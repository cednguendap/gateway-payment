import { FileStorageFactory } from "../fs/FileStorageFactory";
import { InjectorContainer } from "../lifecycle/injector_container";

export function FileStorage()
{
    return (target: any, property: string) => {

        target[property] = InjectorContainer.getInstance()
        .getInstanceOf<FileStorageFactory>(FileStorageFactory)
        .getInstance();;
    }; 
}