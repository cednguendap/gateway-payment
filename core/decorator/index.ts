import { DBPersistence } from "./persistence.decorator"

import { Core } from "./core.decorator"
import { Controller } from "./core.decorator"
import { Service } from "./core.decorator"

import { FileStorage } from "./filestorage.decorator"

import { ConfigService } from "./configuration.decorator"


export {
    FileStorage,
    DBPersistence,
    ConfigService,
    Core,
    Controller,
    Service,
}