/**
@author Cedric nguendap
@description Cette classe est une classe abstraite et classe de base representant l'unite 
    de persistance de type NoSQL (MongoDB, Firebase...)
@created 23/09/2020
*/

import { ConfigurableApp } from "../config/ConfigurableApp.interface";
import { ConfigService } from "../decorator/configuration.decorator";
import { Core } from "../decorator/core.decorator";

import { ApplicationEntity } from "../ApplicationEntity";
import { Entity } from "../Entity";
import { DynamicLoader } from "../utils/DynamicLoader";
import { FileStorage } from "./FileStorage";

@Core()
export class FileStorageFactory extends ApplicationEntity
{
    protected kfs:FileStorage;

    @ConfigService()
    configService:ConfigurableApp

    constructor()
    {
        super();
        this.kfs=DynamicLoader.load(this.configService.getValueOf('persistence').database_file.classe);
    }
    
    /**
     * @inheritdoc
     */
    toString():Record<string | number,any> {
        throw new Error("Method toString() not implemented.");
    }

    /**
     * @inheritdoc
     */
    hydrate(entity: Entity): void {
        throw new Error("Method hydrate() not implemented.");
    }
    
    /**
     * @description permet d'obtenir l'instance de l'unité de stackoge de fichier creer dans le constructeur. cette unité de persistance
     *  est configurer dans le fichier de configuration persistance.json
     * @return une implémentation de l'interface FileStorage
     */
    getInstance():FileStorage 
    {
        return this.kfs;
    }
}