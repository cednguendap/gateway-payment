import { ActionResult } from "../utils/ActionResult";
import { CFile } from "./CFile";



export interface FileStorage
{
    put(file:CFile):Promise<ActionResult>;
    get(name:string):Promise<ActionResult>;
    exist(name:string):Promise<ActionResult>;
}