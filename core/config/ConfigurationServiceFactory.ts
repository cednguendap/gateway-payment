/**
@author: Cedric nguendap
@description: Cette classe est une classe fabrique permettant de fabriquer l'unité 
    de configuration (json, xml,...)
@created: 23/09/2020
*/

import Configuration from "../../config-files/constants";
import { ConfigurableApp } from "./ConfigurableApp.interface";
import { DynamicLoader } from "../utils/DynamicLoader";
import { ApplicationEntity } from "../ApplicationEntity";
import { Entity } from "../Entity";

export class ConfigurationServiceFactory extends ApplicationEntity
{
    protected configService:ConfigurableApp;

    constructor()
    {
        super();
        this.configService=DynamicLoader.load(Configuration.class_for_configuration);
    }
    /**
     * @inheritdoc
     */
    toString():Record<string | number,any> {
        throw new Error("Method not implemented.");
    }

    /**
     * @inheritdoc
     */
    hydrate(entity: Entity): void {
        throw new Error("Method not implemented.");
    }
    
    /**
     * @description permet d'obtenir l'instance de l'unité de configuration créer dans le constructeur
     *   cette unité de configuration est configurer dans le fichier de configuration app.json
     * @return une implémentation de l'interface ConfigurablaApp
     */
    getInstance():ConfigurableApp
    {       
        return this.configService;
    }
}