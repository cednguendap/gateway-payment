/**
@author: Cedric nguendap
@description: classe permettant de gerer les fichiers de configuration 
    de type Xml
@see ConfigurationService
@created: 21/09/2020
*/

import { ConfigurationService } from "./ConfigurationService";

export class XmlFileConfigurationService extends ConfigurationService
{

    /**
     * @inheritdoc
     */
    protected encode(content: String) {
        throw new Error("Method not implemented.");
    }

    /**
     * @inheritdoc
     */
    protected decode(content: any): String {
        throw new Error("Method not implemented.");
    }
    
}
