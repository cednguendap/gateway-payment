/** 
@author: Cedric nguendap
@description: Cette classe represente la classe de base de toute classe de la 
    logique applicative (qui n'est pas une classe persistante)
@created: 21/09/2020
 @see Serializable Entity
*/
import { Entity } from "./Entity";

export abstract class  ApplicationEntity extends Entity {
    
}