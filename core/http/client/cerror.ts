import { Core } from "../../decorator/core.decorator";
import { ApplicationEntity } from "../../ApplicationEntity";
import { Entity } from "../../Entity";
import { CRequest } from "./crequest";
import { CResponse } from "./cresponse";

@Core()
export class CError extends ApplicationEntity
{
    
    request:CRequest=new CRequest();
    response:CResponse=new CResponse();
    message:String="";

    
    toString() 
    {
        return {
           
        }
    }

   
    hydrate(entity: Entity): void {
        throw new Error("Method not implemented.");
    }
}
