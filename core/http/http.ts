import { ActionResult } from "../utils/ActionResult";
import { CRequest } from "./client/crequest";

/**
 * @description Cette classe represente la classe de base de tout client et serveur HTTP 
 * @author Cédric Nguendap
 * @created 17/11/2020
 */
export abstract class Http
{
    abstract sendRequest(request:CRequest):Promise<ActionResult>
}