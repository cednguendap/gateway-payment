/**
@author Cedric nguendap
@description Cette classe est une classe abstraite et classe de base representant l'unite 
    de persistance de type NoSQL (MongoDB, Firebase...)
@created 23/09/2020
*/

import { ConfigurableApp } from "../config/ConfigurableApp.interface";
import { ConfigurationServiceFactory } from "../config/ConfigurationServiceFactory";
import { Core } from "../decorator/core.decorator";

import { ApplicationEntity } from "../ApplicationEntity";
import { Entity } from "../Entity";
import { DynamicLoader } from "../utils/DynamicLoader";
import { NoSqlPersistenceManager } from "./NoSqlPersistenceManager";
import { PersistenceManager } from "./PersistenceManager.interface";

@Core()
export class CPersistenceManagerFactory extends ApplicationEntity
{
    protected persistance:PersistenceManager;

    constructor(protected configServiceFactory:ConfigurationServiceFactory)
    {
        super();
        this.persistance=DynamicLoader.load(this.configServiceFactory.getInstance().getValueOf('persistence').classe,[this.configServiceFactory.getInstance()]);
    }
    
    /**
     * @inheritdoc
     */
    toString():Record<string | number,any> {
        throw new Error("Method toString() not implemented.");
    }

    /**
     * @inheritdoc
     */
    hydrate(entity: Entity): void {
        throw new Error("Method hydrate() not implemented.");
    }
    
    /**
     * @description permet d'obtenir l'instance de l'unité de persistace creer dans le constructeur. cette unité de persistance
     *  est configurer dans le fichier de configuration persistance.json
     * @return une implémentation de l'interface PersistenceManager
     */
    getInstance():NoSqlPersistenceManager | PersistenceManager 
    {
        return this.persistance;
    }
}