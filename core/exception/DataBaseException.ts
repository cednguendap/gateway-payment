/**
@author: Cedric nguendap
@description: Exception lié a l'accés et la manipulation de la bd
@see Exception
@created: 22/09/2020
*/
import {Exception} from './Exception';

export class DataBaseException extends Exception
{
    static DATABAE_DISCONNECTED:Number=-60;
    static DATABASE_UNKNOW_ERROR:Number=-59;
    constructor(code:Number,description:String)
    {
        super(code,"Erreur de communication avec la bd: "+description,description);
    }
}