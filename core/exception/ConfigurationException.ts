/**
@author: Cedric nguendap
@description: Exception lié a la configuration de l'application
@see Exception
@created: 22/09/2020
*/
import {Exception} from './Exception';

export class ConfigurationException extends Exception
{
    static PARSE_FILE:Number=-100;
    static ARGUMENT_IS_NOT_CONFIGURABLE_OBJECT:Number=-99;
    static CONFIGURABLE_KEY_NOT_FOUND:Number=-98;
    static CLASS_CONFIGURATION_NOT_FOUND:Number=97;
    
    constructor(code:Number,description:String)
    {
        super(code,"Erreur de configuration "+description,description);
    }
}