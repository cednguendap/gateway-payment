/**
@author: Cedric nguendap
@description: Exception lié au systéme de fichier de nodejs
@see Exception
@created: 22/09/2020
*/
import {Exception} from './Exception';

export class FileSystemException extends Exception
{
    static FILE_NOT_FOUND:Number=-50;
    static UNKNOW_ERROR:Number=-49;
    static UNABLE_TO_IMPORT_ERROR:Number=-48;
    
    constructor(code:Number,description:String)
    {
        super(code,"Erreur du systéme de fichier: "+description,description);
    }
}