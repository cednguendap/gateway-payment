/*
@author: Cedric nguendap
@description: Ceci est la classe de base de toutes les classes 
@created: 21/09/2020
@see SerializableEntity
*/
import {SerializableEntity} from './SerializableEntity';
import { EntityID } from './utils/EntityID';

export abstract class Entity implements SerializableEntity
{
    /***
     *@inheritdoc
     */
    abstract toString():Record<string | number,any>;

    /**
     * @inheritdoc
     */
    abstract hydrate(entity: Record<string | number,any>):void;
}