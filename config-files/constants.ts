var Configuration=
{
    "app_config_file":"./config-files/app.json",
    "file_config_type":'json',
    'class_for_configuration':"/core/config/JsonFileConfigurationService",
    'path_for_module':"/_module",
    'path_for_bussiness_module':"/bussiness",
    'path_for_bussiness_service': "/services",
    'collections':{
        'applications':'Applications',
        'user':'Users'
    },
     'env_mode':"dev_mode"
    //'env_mode':"prod_mode"
};
export default Configuration;