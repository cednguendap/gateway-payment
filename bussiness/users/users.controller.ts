import { Request, Response } from "express";
import Configuration from "../../config-files/constants";
import { Controller, DBPersistence } from "../../core/decorator";
import { PersistenceManager } from "../../core/persistence/PersistenceManager.interface";
import { ActionResult } from "../../core/utils/ActionResult";
import { User } from "../../services/usermanager/entities/User";
import { ApplicationController } from "../application/application.controller";
import { ApplicationError } from "../application/enum";

@Controller()
export class UsersController
{

    @DBPersistence()
    db:PersistenceManager;

    constructor(private appController:ApplicationController){};

    register(request:Request,response:Response)
    {
        let appToken=request.body.appToken;
        let user:User=new User();
        this.appController.findApplicationByToken(appToken)
        .then((result:ActionResult)=>{            
            user.appToken=appToken;
            return this.db.addToCollection(
                Configuration.collections.user,
                user
            )
        })
        .then((result:ActionResult)=>{
            response.status(201).json({
                code:ActionResult.SUCCESS,
                data:{
                    userID:user.id.toString()
                }
            })
        })
        .catch((error:ActionResult)=>{
            let status=500, code=error.resultCode,message=error.message;
            if(error.resultCode==ActionResult.RESSOURCE_NOT_FOUND_ERROR)
            {
                status=404;
                code=ApplicationError.APPLICATION_NOT_FOUND_ERROR;
            }
            response.status(status).json({
                code,
                message
            })
        })
    }
}