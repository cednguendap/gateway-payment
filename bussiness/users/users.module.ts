import { RouterModule } from "../../core/routing/router_module.decorator";


@RouterModule([
    {
        "url":"/api/app/user/register",
        "module" :UsersModuleRouting,
        "actions":[
            {
                "method":"post",
                "action": "register",
                "isSecure": false,
                "params": {
                    "appToken": "string",
                }
            }
        ]
    },
])
export class UsersModuleRouting{}