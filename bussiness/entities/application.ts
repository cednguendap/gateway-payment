import { CPersistentEntity } from "../../core/persistence/CPersistentEntity";

export class Application extends CPersistentEntity
{
    nom:string="";
    token:string="";
    registrationTime:string="";
}