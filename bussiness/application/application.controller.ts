import Configuration from "../../config-files/constants";
import { Controller, DBPersistence } from "../../core/decorator";
import { PersistenceManager } from "../../core/persistence/PersistenceManager.interface";
import { ApiAccess } from "../../core/security/apiaccess";
import { ActionResult } from "../../core/utils/ActionResult";
import { Application } from "../entities/application";
import { Request, Response } from "express";

@Controller()
export class ApplicationController
{
    @DBPersistence()
    db:PersistenceManager;
    constructor(private jwtAuth:ApiAccess){}

    register(request:Request,response:Response)
    {
        
        let appName=request.body.appName, token=""; 
        this.jwtAuth.JWTRegister({appName,time:new Date().getTime()})
        .then((result:ActionResult)=>{
            token=result.result;
            let app:Application=new Application();
            app.nom=appName;
            app.token=result.result;
            app.registrationTime=(new Date()).toISOString();
            return this.db.addToCollection(Configuration.collections.applications,app)
        })
        .then((result:ActionResult)=>{
            response.status(201).json({
                code:ActionResult.SUCCESS,
                data:{
                    token
                }
            });
        })
        .catch((error:ActionResult)=>{
            response.status(500).json({
                code:error.resultCode,
                message:error.message
            })
        })
    }

    login(request:Request,response:Response)
    {
        
    }

    findApplicationByToken(token:string):Promise<ActionResult>
    {
        return new Promise<ActionResult>((resolve,reject)=>{
            this.db.findInCollection(Configuration.collections.applications, { token })
            .then((result:ActionResult)=>{
                if(result.result.length==0) 
                {
                    result.resultCode=ActionResult.RESSOURCE_NOT_FOUND_ERROR;
                    result.result=null;
                    result.message="";
                    return reject(result);
                }
                let app:Application=new Application();
                app.hydrate(result.result[0]);
                result.result=app;
                resolve(result);
            })
            .catch((error:ActionResult)=>reject(error))
        })
    }
}