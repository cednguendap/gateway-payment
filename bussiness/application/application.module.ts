import { ApplicationController } from "./application.controller";
import { RouterModule } from "./../../core/routing/router_module.decorator"

@RouterModule([
    {
        "url":"/api/app/register",
        "module" :ApplicationController,
        "actions":[
            {
                "method":"post",
                "action": "register",
                "isSecure": false,
                "params": {
                    "email": "string",
                    "password": "string"
                }
            }
        ]
    },
])
export class ApplicationModuleRouting
{

}